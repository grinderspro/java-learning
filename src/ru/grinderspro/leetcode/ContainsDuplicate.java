package ru.grinderspro.leetcode;

import java.util.Arrays;

public class ContainsDuplicate {

    public static void main(String[] args) {

        int[] intArray = {1,2,4,3,5,22,3,9};

        System.out.print(isDuplicate(intArray));
    }

    private static boolean isDuplicate(int[] arrayVal) {
        if(arrayVal == null)
            return false;

        Arrays.sort(arrayVal);

        for(int i =0; i < arrayVal.length - 1; i++) {
            if(arrayVal[i] == arrayVal[i + 1])
                return true;
        }

        return false;
    }
}