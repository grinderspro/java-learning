package ru.grinderspro.leetcode;

public class Palidrome {
    public static void main(String[] args) {
        String strTarget = "Top spot";
        System.out.println(isPalidromeString(strTarget));
    }

    private static boolean isPalidromeString(String palidromeString) {

        String modString = palidromeString.replaceAll("\\s+", "").toLowerCase();

        if(modString.equals("") || modString.length()%2 == 0)
            return false;

        int i = 0;
        int j = modString.length()-1;
        boolean result = true;

        for(i=0; i < (modString.length()/2)-1; i++) {
            if(modString.charAt(i) != modString.charAt(j)) {
                result = false;
                break;
            }
            j--;
        }

        return result;
    }
}