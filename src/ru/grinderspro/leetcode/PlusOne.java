package ru.grinderspro.leetcode;

public class PlusOne {
    public static void main(String[] args) {
        String targetString = "Hello world";
        System.out.println(new Calculator(targetString).calculate());
    }
}

class Calculator {

    private String targetString;

    public Calculator (String targetString) {
        this.targetString = targetString;
    }

    public String calculate() {
        return this.targetString;
    }
}