package ru.grinderspro.employee;

import java.time.LocalDate;

public class Employee {
    private String name;
    private double salary;
    private LocalDate initDate;

    public Employee(String name, double salary, int year, int month, int day) {
        this.name = name;
        this.salary = salary;
        this.initDate = LocalDate.of(year, month, day);
    }
}
